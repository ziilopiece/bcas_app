package com.example.bcasapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasapp.databinding.ActivityHomeBinding
import com.example.bcasapp.view.login.LoginActivity
import com.example.bcasapp.view.login.LoginActivity.Companion.KEY_NAME
import com.example.bcasapp.view.login.LoginActivity.Companion.KEY_PASSWORD
import com.example.bcasapp.view.profile.ProfileActivity

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle :Bundle? = intent.extras
        val name = bundle?.getString(KEY_NAME)
        val password = bundle?.getString(KEY_PASSWORD)

        binding.tvProfile.text = name

        binding.btnProfile.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra(KEY_NAME, name)
            intent.putExtra(KEY_PASSWORD, password)
            startActivity(intent)
        }

    }

    private fun intentTo(clazz: Class<*>){
        val intent = Intent(this, clazz)
        startActivity(intent)
    }
}