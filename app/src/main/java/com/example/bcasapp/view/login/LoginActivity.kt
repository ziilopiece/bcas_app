package com.example.bcasapp.view.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasapp.HomeActivity
import com.example.bcasapp.view.register.RegisterActivity
import com.example.bcasapp.databinding.ActivityLoginBinding
import com.example.bcasapp.view.profile.ProfileActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.tvRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        binding.btnLogin.setOnClickListener {
            val inputUsername = binding.etUsername.text.toString()
            val inputPassword = binding.etPassword.text.toString()

            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.putExtra(KEY_NAME, inputUsername)
            intent.putExtra(KEY_PASSWORD, inputPassword)
            startActivity(intent)
        }

    }

//    private fun navigateScreenWithInput(screen: Class<*>, input: String) {
//        val intent = Intent(applicationContext, screen)
//        intent.putExtra(KEY_NAME, input)
//        intent.putExtra(KEY_PASSWORD, input)
//        startActivity(intent)
//    }

    companion object {
        const val KEY_NAME = "name"
        const val KEY_PASSWORD = "password"
        const val KEY_ADDRESS = "address"
        const val KEY_PHONE = "phone"
    }

}