package com.example.bcasapp.view.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasapp.databinding.ActivityProfileBinding
import com.example.bcasapp.view.login.LoginActivity.Companion.KEY_NAME
import com.example.bcasapp.view.login.LoginActivity.Companion.KEY_PASSWORD

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle :Bundle? = intent.extras
        val name = bundle?.getString(KEY_NAME)
        val password = bundle?.getString(KEY_PASSWORD)

        binding.tvName.text = name
        binding.tvPassword.text = password

    }
}

